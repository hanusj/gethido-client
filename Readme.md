# React-client

Tato aplikace tvoří klientskou část aplikace vývíjené v rámci bakalářské práce na téma "Aplikace funkcionálního programování ve vývoji podnikových aplikací". Aplikace umožňuje jednoduchou správu uživatelů. Hlavním cílem je ilustrovat funkcionální principy.

## Zprovoznění

Následující instrukce umožňují vytvořit spustitelnou lokální kopii pro vývoj nebo testování.

### Předpoklady

Pro běh aplikace je potřeba, aby bylo nainstalováno:
* Apache
* sbt (1.1.4)
* java (1.8.0_162)
* scala (2.12.4)

V závorkách jsou uvedeny verze, které byly využity při vývoji.

Aplikace přistupuje k serverové části, takže pro správnou funkčnost je nutné spustit serverovou část a nakonfigurovat správnou adresu.
Konfigurace se nachází v souboru **src/main/resources/application.conf**. Defaultní hodnota odpovídá defaultní hodnotě serveru.

## Spuštění

Pro spuštění aplikace je nutná kompilace Scala kódu do JavaScript pomocí Scala.js kompilátoru.

**sbt fastOptJS** vytvoří adresářovou strukturu s JavaScript soubory - **js/**.

Pro spuštění je nutné do nastaveného Apache nasadit celý adresář **js/** a **index.html**.

Aplikace je dostupná z webového prohlížeče. Adresa závisí na nastavení Apache (nejspíše http://localhost/).

Pro správnou funkčnost je nutné mít spuštěnou serverovou část a adresa

## Autor

* Jan Hanuš
