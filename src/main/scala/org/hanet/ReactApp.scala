package org.hanet


import org.scalajs.dom

import scala.scalajs.js.annotation.JSExport
import japgolly.scalajs.react.ReactDOM

object ReactApp {


  @JSExport
  def main(args: Array[String]): Unit = {
    import css._
    load

    GethidoRouter.router().renderIntoDOM(dom.document.getElementById("body"))

  }

}
