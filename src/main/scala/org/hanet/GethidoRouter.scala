package org.hanet

import japgolly.scalajs.react.extra.router.{Resolution, RouterConfigDsl, RouterCtl, _}
import japgolly.scalajs.react.vdom.html_<^._
import org.hanet.component.SideMenu
import org.hanet.model.MenuItem
import org.hanet.page._
import org.scalajs
import org.scalajs.dom


object GethidoRouter {

  val routerConfig = RouterConfigDsl[GethidoPage].buildConfig { dsl =>
    import dsl._

    (trimSlashes
      | staticRoute(root, ListUserPage) ~> renderR(r => UserTable(UserTable.Props(r)))
      | staticRoute("#create" , CreateUserPage) ~> render(CreateUserForm())
      | dynamicRouteCT("#edit" / long.caseClass[EditUserPage]) ~> dynRender(EditUserForm.component(_)))
      .notFound(redirectToPage(ListUserPage)(Redirect.Replace))
      .renderWith(layout)
  }

  val menu = Vector(
    MenuItem("User List", ListUserPage),
    MenuItem("Create User", CreateUserPage)
  )

  def layout(c: RouterCtl[GethidoPage], r: Resolution[GethidoPage]) =
    <.div(
      SideMenu(SideMenu.Props(menu, r.page, c)),
      r.render()
    )

  val baseUrl =
    scalajs.dom.window.location.hostname match {
      case "localhost" | "127.0.0.1" | "0.0.0.0" =>
        BaseUrl.fromWindowUrl(_.takeWhile(_ != '#'))
      case _ =>
        BaseUrl.fromWindowOrigin / "gethido/"
    }

  val router = Router(baseUrl, routerConfig)
}
