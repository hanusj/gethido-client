package org.hanet.css

import scalacss.internal.mutable.StyleSheet
import CssSettings._
import japgolly.scalajs.react.vdom.TagMod
object BootstrapStyles extends StyleSheet.Inline {

  import dsl._

  val navMenu = style(display.flex,
    alignItems.center,
    backgroundColor(c"#99ccff"),
    margin.`0`,
    listStyle := "none")

  val menuItem = styleF.bool { selected =>
    styleS(
      padding(20.px),
      fontSize(1.5.em),
      cursor.pointer,
      color(c"#0099cc"),
      mixinIfElse(selected)(backgroundColor(c"#006699"), fontWeight._500)(
        &.hover(backgroundColor(c"#003366")))
    )
  }

  val sidebarHeader = style(
    addClassName("sidebar-header")
  )

  val tooltip = style(
    addClassName("tooltip")
  )

  val validatedForm = style(
    addClassName("needs-validation"),
    paddingTop(20.px)
  )

  val bootstrapButton = style(
    addClassName("btn btn-primary")
  )

  val table = style(
    addClassName("table")
  )

  val helpMsg = style(
    addClassNames("text-muted")
  )

  val formGroup = style(
    addClassName("form-group row")
  )

  val formCol = style(
    addClassName("col-md-4 mb-3")
  )

  val formControl = style(
    addClassName("form-control")
  )

  val formControlValid = style(
    addClassName("form-control is-valid")
  )

  val formControlInvalid = style(
    addClassName("form-control is-invalid")
  )

  val formValidFeedback = style(
    addClassNames("valid-feedback")
  )

  val formInvalidFeedback = style(
    addClassNames("invalid-feedback")
  )

  val formColumn = style(
    addClassName("col-sm-4")
  )

  val formLabel = style(
    addClassNames("col-sm-2 col-form-label"),
    textAlign.center
  )

  val heading = style(
    addClassName("display-4"),
    textAlign.center
  )

}
