package org.hanet

import scalacss.internal.mutable.GlobalRegistry

package object css {

  val CssSettings = scalacss.ProdDefaults

  import CssSettings._

  def load = {
    GlobalRegistry.register(
      BootstrapStyles
    )
    GlobalRegistry.onRegistration(_.addToDocument())
  }
}
