package org.hanet.utils

object Formats {

  private val emailRegex = """^\S+@\S+\.\S+$""".r

  def checkEmail(e: String): Boolean = e match {
    case null                                           => false
    case e if e.trim.isEmpty                            => false
    case e if emailRegex.findFirstMatchIn(e).isDefined  => true
    case _                                              => false
  }

}
