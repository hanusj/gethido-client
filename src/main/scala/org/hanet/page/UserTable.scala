package org.hanet.page

import japgolly.scalajs.react._
import japgolly.scalajs.react.extra.Ajax
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.vdom.html_<^._
import org.hanet.css.BootstrapStyles

import scalacss.ScalaCssReact._
import org.hanet.model.User


object UserTable {

  val component = ScalaComponent.builder[Props]("UserTable")
    .initialState(List.empty[User])
    .renderBackend[Backend]
    .componentDidMount(_.backend.start)
    .build

  case class Props(ctl: RouterCtl[GethidoPage])

  class Backend($: BackendScope[Props, List[User]]) {

    def start = {
      import org.hanet.service.UserService._
      import upickle.default._

      getAll.onComplete(xht =>
        xht.status match {
          case 200  => $.setState(read[List[User]](xht.responseText))
          case _    => Callback.alert(s"Requesting server failed!")
        }
      ).asCallback
    }

    import org.hanet.component.factory.ComponentFactory._

    def render(props: Props, users: List[User]) =
      createUserTable(users, props.ctl)
  }

  def apply(props: Props) = component(props)

}
