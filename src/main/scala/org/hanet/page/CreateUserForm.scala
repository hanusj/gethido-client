package org.hanet.page

import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._
import org.hanet.UserFormUtils
import org.hanet.component.factory
import org.hanet.css.BootstrapStyles
import org.hanet.model.User

import scalacss.ScalaCssReact._

object CreateUserForm {

  import org.hanet.UserFormUtils._

  val component = ScalaComponent.builder[Unit]("UserForm")
    .initialState(UserFormUtils.State())
    .renderBackend[CreateUserForm.Backend]
    .build

  class Backend($: BackendScope[Unit, State]) {

    import org.hanet.service.UserService._
    import upickle.default._

    def onNameChange(e: ReactEventFromInput): Callback = {
      val value = e.target.value
      val validation = nameValidation(value)
      $.modState(s => nameLens.set(nameValidationLens.set(s, validation), value))
    }

    def onEmailChange(e: ReactEventFromInput): Callback = {
      val value = e.target.value
      val validation = emailValidation(value)

      $.modState(s => emailLens.set(emailValidationLens.set(s, validation), value))
    }

    def onPasswordChange(e: ReactEventFromInput): Callback = {
      val value = e.target.value
      val validation =
        passwordValidation(value)
      $.modState(s => passwordLens.set(passwordValidationLens.set(s, validation), value))
    }

    def handleSubmit(user: User) = {
      createUser(user)
        .onComplete { xht =>
          val usr = read[User](xht.responseText)
          $.modState(_.copy(user = User(name = usr.name, email = usr.email, password = usr.password)))
        }.asCallback
    }

    import factory.ComponentFactory._

    def render(state: State): VdomElement =
      <.div(
        <.form(BootstrapStyles.validatedForm,
          ^.onSubmit --> handleSubmit(state.user),
          createFormTextInput("Name", state.user.name, onNameChange, state.nameValidation, Some("Must be 3-40 characters long.")),
          createFormTextInput("Email", state.user.email, onEmailChange, state.emailValidation, Some("Must be in email format.")),
          createFormPasswordInput("Password", state.user.password, onPasswordChange, state.passwordValidation, Some("Must be 8-20 characters long.")),
          <.div(BootstrapStyles.formGroup,
            <.div(BootstrapStyles.formCol),
            createFormButton("Save",
              List(
                ^.disabled := {
                  for {
                    n <- state.nameValidation.valid
                    e <- state.emailValidation.valid
                    p <- state.passwordValidation.valid
                  } yield !(n && e && p)
                }.getOrElse(true)
              )
            )
          )
        )
      )
  }

  def apply() = component()

}