package org.hanet.page

import japgolly.scalajs.react.extra.router.RouterCtl


sealed trait GethidoPage
case object CreateUserPage extends GethidoPage
case class EditUserPage(id: Long) extends GethidoPage
case object ListUserPage extends GethidoPage