package org.hanet.page

import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{BackendScope, Callback, ReactEventFromInput, ScalaComponent}
import org.hanet.UserFormUtils
import org.hanet.component.factory
import org.hanet.css.BootstrapStyles
import org.hanet.model.User
import org.scalajs.dom

import scalacss.ScalaCssReact._

object EditUserForm {

  import org.hanet.UserFormUtils._

  val component = ScalaComponent.builder[EditUserPage]("EditForm")
    .initialState(UserFormUtils.State())
    .renderBackend[EditUserForm.Backend]
    .componentDidMount(p => p.backend.start(p.props.id))
    .build


  class Backend($ : BackendScope[EditUserPage, State]) {

    import org.hanet.service.UserService._

    def onNameChange(e: ReactEventFromInput): Callback = {
      val value = e.target.value
      val validation = nameValidation(value)
      $.modState(s => nameLens.set(nameValidationLens.set(s, validation), value))
    }

    def onEmailChange(e: ReactEventFromInput): Callback = {
      val value = e.target.value
      val validation = emailValidation(value)
      $.modState(s => emailLens.set(emailValidationLens.set(s, validation), value))
    }

    def onPasswordChange(e: ReactEventFromInput): Callback = {
      val value = e.target.value
      val validation =
        passwordValidation(value)
      $.modState(s => passwordLens.set(passwordValidationLens.set(s, validation), value))
    }

    def start(id: Long) = {
      import upickle.default._

      getUser(id).onComplete(xht => {
            xht.status match {
              case 200 => $.modState(userLens.set(_, read[User](xht.responseText)))
              case _ => Callback.alert("Requesting server failed!")
            }
          }
      ).asCallback
    }

    def onNameSubmit(name: String) = {
      $.props.flatMap(p => {
        changeName(p.id, name).onComplete(handleResponse("Name was changed.")).asCallback
      })
    }

    def handleResponse(msg: String)(res: dom.XMLHttpRequest) = res.status match {
      case 200 => Callback.alert(msg)
      case a @ _ => Callback.alert(s"Receive $a from backend!")
    }

    def onEmailSubmit(email: String) = {
      $.props.flatMap(p => {
        changeEmail(p.id, email).onComplete(handleResponse("Email was changed.")).asCallback
      })
    }

    def onPasswordSubmit(password: String) = {
      $.props.flatMap(p => {
        changePassword(p.id, password).onComplete(handleResponse("Password was changed.")).asCallback
      })
    }

    import factory.ComponentFactory._

    def render(state: State): VdomElement =
      <.div(
        <.form(BootstrapStyles.validatedForm,
          createFormTextInputWithSubmit("Name", state.user.name, onNameChange, onNameSubmit, state.nameValidation, Some("Must be 3-40 characters long.")),
          createFormTextInputWithSubmit("Email", state.user.email, onEmailChange, onEmailSubmit, state.emailValidation, Some("Must be in email format.")),
          createFormPasswordInputWithSubmit("Password", state.user.password, onPasswordChange, onPasswordSubmit, state.passwordValidation, Some("Must be 8-20 characters long."))
        )
      )
  }

  def apply(props: EditUserPage) = component(props)
}

