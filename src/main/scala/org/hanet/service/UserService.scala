package org.hanet.service

import org.hanet.model.{ChangeEmail, ChangeName, ChangePassword, Create, User}

import scala.concurrent.Future
import scala.scalajs.js.JSON
import scala.util.Try

trait UserService {

  def getUser(id: Long) = {
    Rest.get(s"user/id/$id")
  }

  def createUser(user: User) = {
    Rest.post(s"user/create", Create(user.name, user.email, user.password))
  }

  def changeName(id: Long, name: String) = {
    Rest.post(s"user/name", ChangeName(id, name))
  }

  def changeEmail(id: Long, email: String) = {
    Rest.post(s"user/email", ChangeEmail(id, email))
  }

  def changePassword(id: Long, password: String) = {
    Rest.post(s"user/password", ChangePassword(id, password))
  }

  def getAll = {
    Rest.get(s"user/all")
  }
}

object UserService extends UserService
