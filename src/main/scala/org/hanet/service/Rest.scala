package org.hanet.service

import com.typesafe.config.ConfigFactory
import japgolly.scalajs.react.extra.Ajax
import upickle.default._

object Rest {

  val url = ConfigFactory.load().getString("server.address")

  private def getUrl(postfix: String) = url ++ postfix

  def get(postfix: String): Ajax.Step2 = {
    Ajax.get(getUrl(postfix)).setRequestContentTypeJsonUtf8.send
  }

  def post[T : Writer](postfix: String, body: T): Ajax.Step2 = {
    val bodyJson = write(body)
    println(bodyJson)
    Ajax.post(getUrl(postfix))
      .setRequestContentTypeJsonUtf8
      .send(bodyJson)
  }
}
