package org.hanet.model

import org.hanet.utils.Lens


case class User(id: Long = 0L, name: String, email: String, password: String)

object User {

  val empty = User(0L, "", "", "")

  val userNameLens = Lens[User, String](get = _.name, set = (u, n) => u.copy(name = n))
  val userEmailLens = Lens[User, String](get = _.email, set = (u, e) => u.copy(email = e))
  val userPasswordLens = Lens[User, String](get = _.password, set = (u, p) => u.copy(password = p))


}