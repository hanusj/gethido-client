package org.hanet.model

sealed trait UserRequest
case class ChangeEmail(id: Long, value: String) extends UserRequest
case class ChangeName(id: Long, value: String) extends UserRequest
case class ChangePassword(id: Long, value: String) extends UserRequest
case class Create(name: String, email: String, password: String) extends UserRequest