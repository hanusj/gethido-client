package org.hanet.model

import org.hanet.page.GethidoPage


case class MenuItem(name: String, route: GethidoPage)
