package org.hanet

import ujson.AbortJsonProcessingException
import upickle.default.Reader
import upickle.default._
import upickle.default.{Reader, macroRW, ReadWriter => RW}

package object model {

  implicit def rwUser: RW[User] = macroRW
  implicit def rwEmail: RW[ChangeEmail] = macroRW
  implicit def rwName: RW[ChangeName] = macroRW
  implicit def rwPsw: RW[ChangePassword] = macroRW
  implicit def rwCreate: RW[Create] = macroRW

}
