package org.hanet.model

case class InputValidation(valid: Option[Boolean] = None, msg: Option[String] = None)
