package org.hanet.model

import upickle.default.{ReadWriter => RW, macroRW}

case class Credentials(name: String, password: String)

object Credentials {
  implicit def rw: RW[Credentials] = macroRW
}
