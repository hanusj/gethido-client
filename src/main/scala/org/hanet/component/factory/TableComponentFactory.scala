package org.hanet.component.factory

import japgolly.scalajs.react._
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.vdom.html_<^._
import org.hanet.css.BootstrapStyles
import org.hanet.model.User
import org.hanet.page.{EditUserPage, GethidoPage}

import scalacss.ScalaCssReact._

trait TableComponentFactory extends ButtonComponentFactory {

  def createUserTable(users: Seq[User], ctl: RouterCtl[GethidoPage]) =
    <.table(BootstrapStyles.table,
      <.thead(
        <.tr(
          <.th("ID"),
          <.th("Name"),
          <.th("Email")
        )
      ),
      <.tbody(
        users.map { u =>
          <.tr(
            <.td(u.id),
            <.td(u.name),
            <.td(u.email),
              <.td(
                createButton("Edit", List(ctl setOnClick EditUserPage(u.id)))
              )
          )
        }.toVdomArray
      )
    )

}
