package org.hanet.component.factory

import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._
import org.hanet.css.BootstrapStyles
import japgolly.scalajs.react.vdom.TagMod
import scalacss.ScalaCssReact._

trait ButtonComponentFactory {

  def createButton(caption: String, tagMod: List[TagMod]) =
    <.button(tagMod ::: List(TagMod(caption), TagMod(BootstrapStyles.bootstrapButton)): _*)

  def createFormButton(caption: String, tagMod: List[TagMod]) =
    <.div(BootstrapStyles.formCol,
      <.button(tagMod ::: List(TagMod(caption), TagMod(BootstrapStyles.bootstrapButton)): _*)
    )
}
