package org.hanet.component.factory


object ComponentFactory extends ButtonComponentFactory
  with CustomComponentFactory
  with InputComponentFactory
  with TableComponentFactory