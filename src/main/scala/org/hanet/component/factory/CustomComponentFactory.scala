package org.hanet.component.factory

import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._
import org.hanet.css.BootstrapStyles
import scalacss.ScalaCssReact._

trait CustomComponentFactory {

  def createHeading(heading: String) =
    <.h3(heading, BootstrapStyles.heading)

}
