package org.hanet.component.factory

import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._
import org.hanet.css.BootstrapStyles
import japgolly.scalajs.react.vdom.TagMod
import org.hanet.css
import org.hanet.model.InputValidation

import scalacss.ScalaCssReact._

trait InputComponentFactory extends ButtonComponentFactory {

  def createFormTextInput(caption: String, value: => String, handler: ReactEventFromInput => Callback, validation: => InputValidation, helpMsg: Option[String] = None) =
    <.div(
        <.label(BootstrapStyles.formLabel, caption),
        <.div(BootstrapStyles.formColumn,
          <.input.text(createInputTagMods(caption, value, handler, validation.valid): _*),
          createValidationMsg(validation)
        ),
      createInputHelpMsg(helpMsg),
      BootstrapStyles.formGroup
    )

  def createFormTextInputWithSubmit(caption: String, value: => String, changeHandler: ReactEventFromInput => Callback, submitHandler: String => Callback,
                                    validation: => InputValidation, helpMsg: Option[String] = None) =
    <.div(
      <.label(BootstrapStyles.formLabel, caption),
      <.div(BootstrapStyles.formColumn,
        <.input.text(createInputTagMods(caption, value, changeHandler, validation.valid): _*),
        helpMsg.whenDefined(m => <.div(BootstrapStyles.tooltip, m)),
        createValidationMsg(validation)
      ),
      <.div(BootstrapStyles.formCol,
        createFormButton("Save", List(^.onClick --> submitHandler(value)))
      ),
      BootstrapStyles.formGroup
    )

  def createFormPasswordInput(caption: String, value: => String, handler: ReactEventFromInput => Callback, validation: => InputValidation, helpMsg: Option[String] = None) =
    <.div(
      <.label(caption, BootstrapStyles.formLabel),
      <.div(BootstrapStyles.formColumn,
        <.input.password(createInputTagMods(caption, value, handler, validation.valid): _*),
        createValidationMsg(validation)
      ),
      createInputHelpMsg(helpMsg),
      BootstrapStyles.formGroup
    )

  def createFormPasswordInputWithSubmit(caption: String, value: => String, changeHandler: ReactEventFromInput => Callback, submitHandler: String => Callback,
                                    validation: => InputValidation, helpMsg: Option[String] = None) =
    <.div(
      <.label(BootstrapStyles.formLabel, caption),
      <.div(BootstrapStyles.formColumn,
        <.input.password(createInputTagMods(caption, value, changeHandler, validation.valid): _*),
        helpMsg.whenDefined(m => <.div(BootstrapStyles.tooltip, m)),
        createValidationMsg(validation)
      ),
      <.div(BootstrapStyles.formCol,
        createFormButton("Save", List(^.onClick --> submitHandler(value)))
      ),
      BootstrapStyles.formGroup
    )

  private def createValidationMsg(validation: => InputValidation) =
    validation.msg.whenDefined(m =>
      <.div(validation.valid.whenDefined(v => if (v) BootstrapStyles.formValidFeedback else BootstrapStyles.formInvalidFeedback),
        m)
    )

  private def createInputHelpMsg(helpMsg: Option[String]) =
    helpMsg.whenDefined(msg =>
      <.div(BootstrapStyles.formCol,
        <.small(msg, BootstrapStyles.helpMsg)
      )
    )

  private def createInputTagMods(caption: String, value: => String, handler: ReactEventFromInput => Callback, valid: => Option[Boolean]): List[TagMod] = {

    val style = valid.map(e =>
      if (e) BootstrapStyles.formControlValid
      else BootstrapStyles.formControlInvalid
    ).getOrElse(BootstrapStyles.formControl)

    List(
      style,
      ^.placeholder := caption,
      ^.value := value,
      ^.onChange ==> handler
    )
  }
}
