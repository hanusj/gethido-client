package org.hanet

import org.hanet.model.{InputValidation, User}
import org.hanet.model.User.{empty, userEmailLens, userNameLens, userPasswordLens}
import org.hanet.utils.{Formats, Lens}

object UserFormUtils {

  case class State(user: User = empty, nameValidation: InputValidation = InputValidation(),
                   emailValidation: InputValidation = InputValidation(),
                   passwordValidation: InputValidation = InputValidation())


  import org.hanet.utils.LensUtils.compose
  val userLens = Lens[State, User](get = _.user, set = (s, u) => s.copy(user = u))

  val nameLens = compose(userLens, userNameLens)
  val emailLens = compose(userLens, userEmailLens)
  val passwordLens = compose(userLens, userPasswordLens)

  val nameValidationLens = Lens[State, InputValidation](get = _.nameValidation, set = (s, v) => s.copy(nameValidation = v))
  val emailValidationLens = Lens[State, InputValidation](get = _.emailValidation, set = (s, v) => s.copy(emailValidation = v))
  val passwordValidationLens = Lens[State, InputValidation](get = _.passwordValidation, set = (s, v) => s.copy(passwordValidation = v))

  def nameValidation(value: String) = {
    if (value.length < 3 || value.length > 20)
      InputValidation(Some(false), Some("Invalid name length!"))
    else
      InputValidation(valid = Some(true))
  }

  def emailValidation(value: String) = {
    if (Formats.checkEmail(value))
      InputValidation(valid = Some(true))
    else
      InputValidation(Some(false), Some("Invalid email format!"))
  }

  def passwordValidation(value: String) = {
    if (value.length < 5 || value.length > 40)
      InputValidation(Some(false), Some("Invalid password!"))
    else
      InputValidation(valid = Some(true))
  }

}
