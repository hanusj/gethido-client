package org.hanet.component

import scalacss.ScalaCssReact._
import japgolly.scalajs.react._
import japgolly.scalajs.react.extra.Reusability
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.vdom.html_<^._
import org.hanet.css.BootstrapStyles
import org.hanet.model.MenuItem
import org.hanet.page.GethidoPage

object SideMenu {

  case class Props(menus: Vector[MenuItem],
                   selectedPage: GethidoPage,
                   ctrl: RouterCtl[GethidoPage])

  implicit val currentPageReuse = Reusability.by_==[GethidoPage]
  implicit val propsReuse = Reusability.by((_: Props).selectedPage)

  import org.hanet.component.factory.ComponentFactory._

  val component = ScalaComponent
    .builder[Props]("SideNav")
    .render_P { P =>
      <.nav(
        <.ul(BootstrapStyles.navMenu,
          P.menus.toTagMod { item =>
            <.li(
              ^.key := item.name,
              BootstrapStyles.menuItem(item.route.getClass == P.selectedPage.getClass),
              item.name,
              P.ctrl setOnClick item.route
            )
          }
        )
      )
    }
    .configure(Reusability.shouldComponentUpdate)
    .build

  def apply(props: Props) = component(props)

}
